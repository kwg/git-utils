#! /usr/bin/python3
#
# Copyright © 2016 Kenneth Graunke
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

import os
import os.path as path
import sys
import argparse
from datetime import datetime
from babel.dates import format_timedelta
from pygit2 import *
import colorama
import pydoc

def commit_time(repo, args, ref):
    commit = repo.lookup_reference(ref).peel()
    if args.author:
        return commit.author.time
    else:
        return commit.commit_time

def branch_string(branch, timestamp):
    delta = datetime.now() - datetime.fromtimestamp(timestamp)
    date = format_timedelta(delta, threshold=1.8)

    color = colorama.Fore.BLUE + colorama.Style.BRIGHT
    reset = colorama.Style.RESET_ALL

    return '{}{:>22}{} {}'.format(color, date, reset, branch)

def init_color(args):
    if args.color == 'always':
        args.color = True
    elif args.color == 'never':
        args.color = False
    elif args.color == 'auto':
        args.color = None

    colorama.init(strip=None if args.color is None else not args.color)

def init_pager():
    if 'LESS' not in os.environ:
        os.environ['LESS'] = 'FRX'
    if 'LV' not in os.environ:
        os.environ['LV'] = '-c'

    if 'GIT_PAGER' in os.environ:
        os.environ['PAGER'] = os.environ['GIT_PAGER']
    else:
        try:
            os.environ['PAGER'] = Config.get_global_config()['core.pager']
        except:
            pass

def find_git_root(start_path):
    pwd = start_path.split(os.sep)
    while pwd:
        if path.exists(path.join(os.sep, *pwd, '.git')):
            return path.join(os.sep, *pwd)
        pwd.pop()
    raise Exception("fatal: Not a git repository")

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--color', choices=['always', 'never', 'auto'])
    parser.add_argument('--no-color', action='store_false', dest='color')
    parser.add_argument('--author', action='store_true')
    # XXX: move to a shared module
    parser.add_argument('remote', nargs='?')
    args = parser.parse_args()

    init_color(args)
    init_pager()

    repo = Repository(find_git_root(os.getcwd()))

    if args.remote:
        ref = 'refs/remotes/'
        branches = repo.listall_branches(GIT_BRANCH_REMOTE)
        branches = [x for x in branches if x.startswith(args.remote + '/')]
    else:
        ref = 'refs/heads/'
        branches = repo.listall_branches()

    branch_times = [(b, commit_time(repo, args, ref + b)) for b in branches]
    lines = [branch_string(b, t) for b, t in sorted(branch_times, key=lambda x: x[1], reverse=True)]
    pydoc.pager('\n'.join(lines))

if __name__ == '__main__':
    main()

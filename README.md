
git age
-------

Lists all current branches, similar to `git branch`, but also showing
when each branch was last modified.  Sorted with recent branches at the top.

For example:

```
           104 minutes master
                2 days iris-rgbx
                3 days l3-credit
                4 days icl-flush
                4 days frames-in-flight
               3 weeks packed-pix-debug
               6 weeks brw-recovery
             21 months bats
               3 years ivbborder
               3 years unlimited-scratch
               7 years value-visitor
```

Dependencies:

* python-pygit2
* python-babel
* python-colorama
